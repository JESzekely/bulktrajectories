#!/bin/bash

if [ 'x' == 'x'$1 ]; then
	echo "Please provide a [quest account]"
	exit
fi

if [ ! -d "output_data" ]; then
	mkdir output_data
fi

cd output_data

for INTEN in {7..14}; do
	echo $INTEN
	DIR="TrajectoriesInten1e"$INTEN
	if [ ! -d $DIR ]; then
		mkdir $DIR
	fi
	cp ../*.txt $DIR
	cp ../*.py $DIR
	cp ../*.json $DIR

	CODEPATH=`pwd`/$DIR
	echo "
	#MOAB -l nodes=1:ppn=1
  	#MOAB -l walltime=4:00:00
  	#MOAB -N Intensity1e$INTEN
  	#MOAB -j oe
  	#MOAB -A $1

  	module load python

 	cd $CODEPATH
 	chmod +x BulkTrajectories.py
 	./BulkTrajectories.py 1.0e$INTEN 200 800.0 4.585 37.223 $INTEN
	" > $DIR/RunFile.sh
	msub $DIR/Runfile.sh
done
