#!/usr/bin/env python

import FDTD
import Inputs as IP
import Rotational_PES as RPES
import constants as C

import numpy as np
import sys
from scipy.integrate import odeint

if len(sys.argv) != 7:
	print "Please provide the following arguments: [laser intensity] [number of velocities] [velocity maximum] [initial x] [initial y] [proc flag]"
	exit()

#Import the FDTD Array
A = FDTD.Array2D("inputs.json")
A.loadTextFile("AgFilmX-3col.txt")
A.transpose()


#Get the alignment functions
alignFXN,enFXN = RPES.RotationalFxn(A.params,A.molparams)
A.getAlignment(alignFXN,A.params['FDTDField']['laser_power']/C.LASERINTEN)
A.getRotationalEnergy(enFXN,A.params['FDTDField']['laser_power']/C.LASERINTEN)

distance2au = A.params['FDTDField']['meep_a']/C.LEN
maxX = max(A.X)
maxY = max(A.Y)
aval = A.params['FDTDField']['meep_a']/C.LEN
res  = A.params['FDTDField']['meep_res']/C.LEN

#Get the particle mass, convert to au
mass =  A.molparams[A.params['MoleculeParameters']['Molecule']]['Mass1'] + A.molparams[A.params['MoleculeParameters']['Molecule']]['Mass2']
mass *= 1836.0

def ClassicalHamiltonian(y,t):
	ex,ey = getLocalGradient(y[0],y[1],t)
	return [y[2]/mass, y[3]/mass,-1.0*ex,-1.0*ey]

def intensityProfile(t):
	return 1.0
	#return 1.0*(0.5*(np.tanh((t-3.5e7/2.41888432e-2)/(3.5e7/2.41888432e-2/20.0)) + 1.0))+1

def getLocalGradient(x,y,t):
	x /= distance2au
	y /= distance2au
	N     = 3
	dx    = dy = 0.50/N
	X     = np.linspace(x-dx*N,x+dx*N,2*N+1)
	Y     = np.linspace(y-dy*N,y+dy*N,2*N+1)
	inten = A.params['FDTDField']['laser_power']
	sx    = maxX
	sy    = maxY
	ERot  = np.zeros((2*N+1,2*N+1))
	print("%f %f %f %e %f" % (t,x,y,enFXN(abs(A.E(x%sx,y%sy))*inten*intensityProfile(t)/C.LASERINTEN),alignFXN(abs(A.E(x%sx,y%sy))*inten*intensityProfile(t)/C.LASERINTEN)))
	for xxx in range(2*N+1):
		for yyy in range(2*N+1):
			ERot[xxx][yyy]  = enFXN(abs(A.E(X[xxx]%sx,Y[yyy]%sy))*inten*intensityProfile(t)/C.LASERINTEN)
	gx,gy = np.gradient(ERot,dx*distance2au,dy*distance2au)
	return gx[N][N],gy[N][N]

inten      = float(sys.argv[1])
x0         = float(sys.argv[4])
y0         = float(sys.argv[5])
velocities = np.linspace(0.0,float(sys.argv[3])/2.18769e6, int(sys.argv[2]))

def GetTrajectory(intensity,xpos,ypos,vel,ang):
	A.params['FDTDField']['laser_power'] = intensity
# Approximate the trajectory time
	tmax = 100.0*1064.0*np.sqrt(mass / (2.0*(intensity/C.LASERINTEN* A.molparams[A.params['MoleculeParameters']['Molecule']]['Polarizability']['XX'] + 0.5*mass*pow(vel,2) )))
	print tmax
	t        = np.linspace(0,tmax,2001)
	px0      = mass*vel*np.cos(ang)
	py0      = mass*vel*np.sin(ang)
	ICBackup = [xpos,ypos,px0,py0]
	ICs      = ICBackup[:]
	ICs[0]   *= distance2au
	ICs[1]   *= distance2au
	traj     = odeint(ClassicalHamiltonian,ICs,t)
# Analysis
	X = [y[0]/distance2au for y in traj]
	Y = [y[1]/distance2au for y in traj]
# It the molecule in the bounds?
	if (max(X) > 40.0 and min(X) < 26.0):
		isBounded = False
	else:
		isBounded = True
# Is it trapped?
	xSpan = (max(X[1000:]) - min(X[1000:]))
	ySpan = (max(Y[1000:]) - min(Y[1000:]))
	if xSpan < 8.0 and ySpan < 8.0 and isBounded:
		isTrapped = True
	else:
		isTrapped = False
	return [traj,isBounded,isTrapped,xSpan,ySpan]

flag = str(sys.argv[6])
outFile = open("TrajectoryData" + flag + ".txt","w")
for velocity in velocities:
	for angle in np.linspace(0.0,2*np.pi,32):
		Trajectory,isB,isT,xS,yS = GetTrajectory(inten,x0,y0,velocity,angle)
		outFile.write("%.6f %.6f %.6e %.6f %.6f %.6f %d %d\n" % (x0,y0,velocity,angle,xS,yS,int(isB),int(isT)))
outFile.close()